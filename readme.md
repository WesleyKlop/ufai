# UFAI

Using functions as input (UFAI) is a new state of the art design pattern that'll take the industry by storm.
This repo is dedicated to provide examples for this pattern in as many languages as possible!

Feel free to contribute!