<?php

namespace Ufai;

class Gore {

	public function __call($func, $args) {
	    $var_name = $this->getRefName();
		$name = sprintf("str_%s", $var_name);

		if(function_exists($name)) {
			return $name($func);
		}

		$name = sprintf("str%s", $var_name);

		if(function_exists($name)) {
			return $name($func);
		}

		return false;
	}

	private function getRefName() {
        $db = debug_backtrace();
        $var_array = [];

        $file = $db[1]["file"];
        $line = $db[1]["line"];

        $content = file($file)[$line-1];
        $pos = strpos($content, '->');
        do {
            $pos -= 1;
            $char = substr($content, $pos, 1);
            array_push($var_array, $char);
        } while ($char != '$' || $pos < 1);

        $name = implode(array_slice(array_reverse($var_array), 1));

        return $name;
    }

}

